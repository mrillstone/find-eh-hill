import React, { useState, useEffect } from 'react'
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api'

export const MapContainer = () => {
    const [ currentPosition, setCurrentPosition ] = useState({});
    const [ id, setId ] = useState(0);
    const [ markers, setMarkers ] = useState([]);
    const locations = [];

    // Current Position
    const success = position => {
        const currentPosition = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        }
        setCurrentPosition(currentPosition);
    };

    // Draggable Marker
    const onMarkerDragEnd = (e) => {
        const lat = e.latLng.lat();
        const lng = e.latLng.lng();
        setCurrentPosition({ lat, lng})
    };

    // Add Marker to Map
    const addMarker = (e) => {
        setId((id) => id + 1);
        const position = {
            lat: e.latLng.lat(),
            lng: e.latLng.lng()
        }
        setMarkers({ position, id })
    };

    useEffect(() => {
        navigator.geolocation.getCurrentPosition(success);
    })

  const mapStyles = {        
    height: "100vh",
    width: "100%"};

  return (
     <LoadScript
       googleMapsApiKey='AIzaSyCw1HW6cmcdMPGv0_Y0Kz1EXrex5FVgTM0'>
        <GoogleMap
          mapContainerStyle={mapStyles}
          zoom={13}
          center={currentPosition}
          onClick = {(e) => addMarker(e)}>
{        
        
        /* {markers ? (
            markers.map((marker) => {
                return (
                    <Marker
                        key={marker.id}
                        draggable={drawMarker}
                        position={marker.coords}
                        onDragEnd={e => marker.coords = e.latLng.toJSON()}
                    />
                )
            })
        ) : null }
            // currentPosition.lat ?
            // <Marker
            //     position={currentPosition}
            //     onDragEnd={(e) => onMarkerDragEnd(e)}
            //     draggable={true} /> :
            //     null
           */       

            markers ? 
            markers.map(item => {
              return (
              <Marker 
                key={item.id} 
                position={item.position}
                draggable={true}
              />
              )
            }) : null

         /*
        {
            selected.location && 
            (
              <InfoWindow
              position={selected.location}
              clickable={true}
              onCloseClick={() => setSelected({})}
            >
              <p>{selected.name}</p>
            </InfoWindow>
            )
         } */}
        </GoogleMap>
     </LoadScript>
  )
}

export default MapContainer;